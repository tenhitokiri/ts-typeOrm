import { Request, Response } from 'express';
import { User } from '../entity/User';
import { getRepository } from 'typeorm';
import { resourceUsage } from 'process';

export const getUsers = async (req: Request, res: Response): Promise<Response> => {
	const users = await getRepository(User).find();
	return res.json(users);
};

export const createUser = async (req: Request, res: Response): Promise<Response> => {
	const newUser = getRepository(User).create(req.body);
	const results = await getRepository(User).save(newUser);
	return res.json(results);
};

export const getUser = async (req: Request, res: Response): Promise<Response> => {
	const user = await getRepository(User).findOne(req.params.id);
	if (user) {
		return res.json(user);
	} else {
		return res.status(404).json({ err: 'not found' });
	}
};

export const updateeUser = async (req: Request, res: Response): Promise<Response> => {
	const user = await getRepository(User).findOne(req.params.id);
	if (user) {
		getRepository(User).merge(user, req.body);
		const result = await getRepository(User).save(user);
		return res.json(result);
	} else {
		return res.status(404).json({ err: 'not found' });
	}
};

export const deleteUser = async (req: Request, res: Response): Promise<Response> => {
	const user = await getRepository(User).delete(req.params.id);
	if (user) {
		return res.json(user);
	} else {
		return res.status(404).json({ err: 'not found' });
	}
};
